#################
Effiloché de porc
#################

.. post::
    :tags: porc, BBQ, pas cher
    :category: bouffe
    :excerpt: 1
    :nocomments:

Quand t'es pas pressé et que tu vas recevoir de la visite, fait cet effiloché de
porc.

.. admonition:: Source

    Cette recette est copiée ou inspirée de celle de `Hungry Rachel`_.

    .. _Hungry Rachel: https://hungryrachel.ca/2014/06/01/effiloche-de-porc-slow-and-low-that-is-the-tempo/

Dans le cochon, tout est bon, même l'épaule!  Pour faire ce délice, commencez la veille.


Matériel
========

-   Une rôtissoire profonde.  J'utilise une grosse foncée pour la dinde.

-   Quelque chose d'assez grand pour submerger une épaule de porc au complet
    et au frigo.  J'utilise des sacs refermables.

-   Optionnellement un moulin électrique (à café, par exemple) ou quoi que ce
    soit pour moudre des graines dûres.

-   Optionnellement des grands cure-dents ou baguettes à brochettes.
    La viande doit cuire «debout» et un tuteur ou deux pourraient l'aider.

-   Un thermomètre à viande


.. _rub-pulled-porc:

La marinade
===========

On fait une marinade sèche, un *rub*.  Ça ressemble à beaucoup de poudre qui
goûte fort.

*   15 ml de cumin moulu
*   15 ml de poudre d’ail
*   15 ml de poudre d’oignon
*   15 ml de poudre de chili
*   15 ml de poivre de Cayenne
*   15 ml de sel
*   15 ml de poivre
*   15 ml de paprika
*   125 ml de cassonade

Mélanger et réserver.

.. tip::

    Le **cumin moulu** soi-même a un goût nettement plus parfumé et intéressant.
    Par contre, pour l'avoir essayé, ces petites graines sont coriaces et les
    moudre au mortier est un travail ardu.  Optez plutôt pour un moulin à café
    électrique.

    En épicerie, la **poudre de chili** dits «*mexicain*» est faite de piment fumés.
    Ça goûte très différent de la poudre de chili «*normale*», mais c'est bon
    aussi.

    Comme la poudre de chili, le **paprika** se vend aussi en mode «*fumé*» si ça
    vous chante.

    Pour cette recette, vous pouvez improviser un peu sur le *rub*.
    L'important c'est de ne pas en manquer.
    Si votre épaule est gigantesque, forcez-dont un peu les quantités!


La saumure
==========

Mettez le porc en saumure pendant au moins 8 heures avant la cuisson.

*   4 l d'eau froide
*   250 ml de sel
*   250 ml de cassonade
*   45 ml de :ref:`rub-pulled-porc`
*   2 feuilles de laurier

La partie la plus difficile est de dissoudre le sel et le sucre dans de l'eau
froide.

#.  Dissoudre le sel et le sucre dans l'eau
#.  Mélanger les autres ingrédients dans l'eau salée et sucrée
#.  Submerger l'épaule dans la saumure (dans un sac refermable, par exemple)
#.  Réfrigérer pendant au moins 8 heures, probablement moins de 48 heures
    serait plus sécuritaire.


Cuire le monstre
================

Si vous voulez manger ce cochon pour diner, cuisez le la veille ou pendant la nuit!
Le temps de cuisson va varier énormément en fonction de la taille de votre pièce
entre 7 et 13 heures, sans compter les heures de repos.

#.  Chauffer le four à 225°F
#.  Sortier et sécher l'épaule avec un linge à vaisselle
#.  Masser la viande avec la marinade.  Mettez-en et frottez bien!
#.  Faire tenir la viande debout la couenne en l'air dans votre rôtissoire
#.  Piquer le thermomètre dans la partie épaisse mais ne touchez pas l'os
#.  Cuire sans couvercle jusqu'à ce que le thermomètre indique 200°F
#.  Éteindre le four, couvrir la viande et la laisser reposer au four
    jusqu'à ce que le thermomètre indique 170°F.

