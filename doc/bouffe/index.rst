.. _cooking:

######
Bouffe
######

Ce n'est pas grand chose, mais c'est de l'honnête bouffe.


Guides et références
====================

.. toctree::
    :maxdepth: 2

    mesure


Recettes
========

.. postlist:: 5
    :excerpts:
    :category: bouffe