.. _bouffe-mesure:

#####################
Mesures et conversion
#####################

Je cuisine toujours avec une balance électronique.  Je trouve que c'est plus
simple de regarder l'écran digital que de me pencher pour approximer ma farine
dans une tasse à mesurer.

Pour référence, voici quelques guides

.. list-table:: Cuillerées et tasses
    :header-rows: 1

    *   *   Mesure
        *   Abbréviation française
        *   Abbréviation anglaise
        *   Volume
    *   *   Cuillère à soupe (ou à table)
        *   c. à soupe ou cs
        *   Tbsp
        *   15 ml
    *   *   Cuillère à thé (à café en France)
        *   c. à thé (resp. à café) ou cc
        *   Tsp
        *   5 ml
    *   *   Tasse
        *   tasse?
        *   cup
        *   250 ml