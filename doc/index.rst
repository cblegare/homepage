##################
La page de Charles
##################

Salut! Découvrez-en plus à mon sujet ici: :ref:`about`.


Voici une liste d'articles récents

.. postlist:: 5
    :excerpts:


Table des matières
==================

.. toctree::
    :maxdepth: 2

    about
    bouffe/index
    posts/index


Index et tableaux
=================

*   :ref:`genindex`
*   :ref:`modindex`
*   :ref:`search`
