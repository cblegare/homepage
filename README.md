![Build Status](https://gitlab.com/cblegare/homepage/badges/master/build.svg)


Getting started
===============

Debian packages required
------------------------

```shell
apt-get update -y && apt-get install fonts-freefont-otf texlive-xetex latexmk -y
```

Python packages required
------------------------

```shell
pip install -U -r requirements.txt
```

Build
-----

Collect translatable messages

```shell
sphinx-build \
    -b gettext \
    -d build/sphinx/doctrees \
    -E -n -W --keep-going -T  \
    doc \
    build/sphinx/gettext
```

Generate or update translated files


```
sphinx-intl -c doc/conf.py update \
    -p build/sphinx/gettext \
    -l fr -l en
```

Build for all languages for all targets

```
for lang in fr en
do
  for target in html 
  do
    sphinx-build \
      -b ${target} \
      -d build/sphinx/doctrees \
      -E -n -W --keep-going -T  \
      -D language=${lang} \
      doc \
      build/sphinx/${lang}/${target}
  done
done
```
